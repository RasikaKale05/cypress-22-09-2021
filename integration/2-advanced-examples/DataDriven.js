/// <reference types="cypress" />

describe('Data Driven Test', function()
{
    before(function(){
        cy.fixture('example').then(function(data){
            this.data = data
        })
    })
    
    it('Verify a Login', function() 
    {
        cy.visit('https://dev-cbt.dilipoakacademy.com/#/login')
        cy.get('#mat-input-0').should('be.visible').should('be.enabled').type(this.data.Mobile)
        cy.get('#mat-input-1').should('be.visible').should('be.enabled').type(this.data.password)
        cy.get(".mat-primary[type='button']").click()
        cy.get(6000)
        cy.get("ul > :nth-child(3)").click()// click on Question Bank
        cy.get(".mat-primary[type='button']").click()// click on Add Question
        cy.get("#mat-input-2").should('be.visible').type(this.data.Reference_id)
        cy.get('#mat-select-1 > .mat-select-trigger > .mat-select-arrow-wrapper').click()//Difficulty Level
        cy.get('.mat-option-text').contains(this.data.Level).click()
        cy.get("#mat-select-2 > .mat-select-trigger > .mat-select-arrow-wrapper").click()//Question For
        cy.get(".mat-option-text").contains(this.data.Question_For).click()
        cy.get("#mat-select-3 > .mat-select-trigger > .mat-select-arrow-wrapper").click()//Section
        cy.get(".mat-option-text").contains(this.data.Section).click()
        cy.get("#mat-select-4 > .mat-select-trigger > .mat-select-arrow-wrapper").click()//Category
        cy.get(".mat-option-text").contains(this.data.Category).click()
        cy.get("#mat-select-7 > .mat-select-trigger > .mat-select-arrow-wrapper").click()
        cy.get(".mat-option-text").contains(this.data.tag1).click()
        cy.get(".mat-option-text").contains(this.data.tag2).click()
        cy.get("[title='Next'] > .mat-raised-button").click({force: true})// Next button
        cy.get('.mat-radio-label-content').contains(this.data.Radio).click()// Select Radio Button
        //upload Question image
             const image = this.data.image_path;
             cy.get('input[type=file]').attachFile(image)

             cy.get('.dialog-error-action > app-iro-button > .mat-raised-button').click()
         cy.get('#mat-select-8 > .mat-select-trigger > .mat-select-arrow-wrapper > .mat-select-arrow').click()
         cy.wait(5000)
         cy.get('.mat-option-text').contains(this.data.OptionA).click()
         const image1 = this.data.Image;
         cy.get('[fxflex="75"] > input[type=file]').attachFile(image1)
         cy.get('.dialog-error-action > app-iro-button > .mat-raised-button').click()
         cy.wait(1000)
         cy.get('#mat-input-4').should('be.visible').should('be.enabled').type(this.data.OptionB)         
         cy.wait(1000)
         cy.get('#mat-input-5').should('be.visible').should('be.enabled').type(this.data.OptionC)
         cy.get('.mat-checkbox-label').contains(this.data.Ans).click()
         cy.wait(5000)
         cy.get('iframe#mce_1_ifr').then(function($iframe)
         
         {
            const iframecontent = $iframe.contents().find('body')
            cy.wrap(iframecontent).type(this.data.justification)
         })
          cy.get('[title="Next"] > .mat-raised-button').click()
          cy.get('[colored="accent"] > .mat-raised-button > .mat-button-wrapper').click()
     })
})