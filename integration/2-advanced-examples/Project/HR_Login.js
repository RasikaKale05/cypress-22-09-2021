/// <reference types="cypress" />

describe('website testing', function()
{
    before(function(){
        cy.fixture('HR_Data').then(function(data){
            this.data = data
       })
    })
    it('Verify a Website', function() 
    {
        cy.visit('http://3.81.251.227/#/login') // open website (Users_Info
        cy.get('#mat-input-1').should('be.enabled').should('be.visible').type(this.data.Email)//enter Email
        cy.get('#mat-input-0').should('be.enabled').should('be.visible').type(this.data.Password)//Enter Password
        cy.get('.mat-focus-indicator').should('be.enabled').should('be.visible').click()// click Login button
        cy.wait(5000)
        //Click  on Action button
        cy.get('app-button[name="Action"]').click()
        //click Create 
        if(this.data.Action=="Create")
        {
            cy.get('.mat-menu-content > :nth-child(1)').click()
            cy.get('#mat-input-5').type('XYZ')//Add title
            // Create (Leave) 
            if(this.data.Create_Type== "Leave")
            {
            cy.get('.mat-radio-label').contains('Leave').click()// click on leave
            cy.get('.mat-radio-label').contains(this.data.Leave_type).click()//select Leave type
            // Select Start date 
            cy.get('#mat-input-2').click()
            cy.get('.mat-calendar-period-button').click()
            cy.get('td.ng-star-inserted').contains(this.data.S_Year).click()
            cy.get('td.ng-star-inserted').contains(this.data.S_Month).click()
            cy.get('td.ng-star-inserted').contains(this.data.S_Day).click()
            //Select End date
            cy.get('#mat-input-3').click()
            cy.get('.mat-calendar-period-button').click()
            cy.get('td.ng-star-inserted').contains(this.data.E_Year).click()
            cy.get('td.ng-star-inserted').contains(this.data.E_Month).click()
            cy.get('td.ng-star-inserted').contains(this.data.E_Day).click()
            // Enter invite People email
            cy.get('#mat-chip-list-input-0').type(this.data.Invite_People)
            //Enter Description
            cy.get('#mat-input-4').type(this.data.Add_Discription)
            //Click on Submit button
           // cy.get('[name="Submit"] > .mat-focus-indicator').click()
            }
            if(this.data.Create_Type== "Event")
            {
            cy.get('.mat-radio-label').contains('Event').click()// click on Event
            cy.get('.mat-radio-label').contains(this.data.Frequency).click()// click on Frequency
            cy.get('#mat-input-2').click()
            cy.get('.mat-calendar-period-button').click()
            cy.get('td.ng-star-inserted').contains(this.data.S_Year).click()
            cy.get('td.ng-star-inserted').contains(this.data.S_Month).click()
            cy.get('td.ng-star-inserted').contains(this.data.S_Day).click()
            //Select End date
            cy.get('#mat-input-3').click()
            cy.get('.mat-calendar-period-button').click()
            cy.get('td.ng-star-inserted').contains(this.data.E_Year).click()
            cy.get('td.ng-star-inserted').contains(this.data.E_Month).click()
            cy.get('td.ng-star-inserted').contains(this.data.E_Day).click()
            // Enter invite People email
            cy.get('#mat-chip-list-input-0').type(this.data.Invite_People)
            //Enter Description
            cy.get('#mat-input-4').type(this.data.Add_Discription)
            //Click on Submit button
            // cy.get('[name="Submit"] > .mat-focus-indicator').click()
            }
        }
        else
        {
        //Click on invite
         cy.get('.mat-menu-content > :nth-child(2)').click()
         cy.get('#mat-chip-list-input-0').type(this.data.Invite_People)// invite people
         //cy.get('[name="Submit"] > .mat-focus-indicator').click()// click on submit button
         cy.get('[name="Cancel"] > .mat-focus-indicator').click()
         }
         cy.get(':nth-child(1) > .padding-ltr-20 > .c-royalblue').click()// click on HR Profile(View Profile)

         cy.get("div[innertext='Educational Details']").should('eq',"Educational Details").click()  // click on HR Educationa; details      
    })
})

